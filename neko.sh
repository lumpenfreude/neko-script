#!/bin/sh
curl -sSL https://get.docker.com/ | CHANNEL=stable bash &&
curl https://gitlab.com/lumpenfreude/neko-script/-/raw/master/docker-compose.yaml --output docker-compose.yaml &&
docker run -p 80:8080 -p 59000-59100:59000-59100/udp -e NEKO_PASSWORD='neko' -e NEKO_ADMIN='admin' --cap-add SYS_ADMIN --shm-size=1gb nurdism/neko:chromium