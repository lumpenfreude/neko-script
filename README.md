# Reamde

## neko.sh -- Running n.eko made extremely simple!

### Know what you're doing? Have a VPS up and running and just want n.eko going? Gotcha.

`sh -c "$(curl -fsSL https://gitlab.com/lumpenfreude/neko-script/-/raw/master/neko.sh)" &`

### For everyone else: 

First, go to [Linode](https://www.linode.com/?r=471210bcc3bed5bcca84b3b33016bb22d086944b) and sign up. This link'll give ya $20 of credit, it's mine so it'll give ME $20 credit too if you spend like $15 or something so please do if you want lol. Once you've signed up hit the "create"
button.

![linode create](img/linode1.png "linode create")


then ya want a debian 10 server with settings something like this. maybe a little better if you plan on streaming at 720p 60fps, MUCH better if you want 1080p. but. this is good for 720p30 (the default setting for n.eko)
you can pick a region closer to yourself as well! add a password (remember it! you'll need it in like. 30 seconds!)


![linode settings](img/linode2.png "linode2 settings stuff")


from there click create. it'll take you to the server page and show you the creation progress. then click the "launch console" button.


![linode console](img/linode2.5.png "linode console i guess")


you'll get into the console. login as "root" with the password you just chose. the password screen **will not show any input** this is normal. don't worry lol. you'll see a shell screen. copy and paste this line into it and hit enter. 


`sh -c "$(curl -fsSL https://gitlab.com/lumpenfreude/neko-script/-/raw/master/neko.sh)" &`


![console login](img/linode4.png "linode loginthing")


it'll do some stuff, download some stuff. wait a bit. then you'll see garbage like this! openbox running! chromium running! good shit. cool. you're basically done now lmao. go to your ip address! you should see a neko login. login with whatever you want and the password "admin" and you're in business! invite your friends, they can login with whatever they want and the password "neko"


![console final](img/linode5.png "linode garbge")


when you're all done, if you want to destroy the server and save yrself some money, go back to the products page and go over to the "settings" menu and hit "destroy." fuckin NUKE THAT SHIT BAY-BEE. lmao done! you just spent like idk, 40 cents? dope! enjoy many many more movie nights on that $20 credit! thanks shelby aka akbkuku of youtube lol. 


![linode destroy](img/linode6.png "kill")

